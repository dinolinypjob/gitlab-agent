load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "gitlab.proto",
    workspace_relative_target_directory = "internal/gitlab",
)

go_library(
    name = "gitlab",
    srcs = [
        "client.go",
        "client_options.go",
        "do_options.go",
        "error.go",
        "gitlab.pb.go",
        "response_handlers.go",
        "types.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v15/internal/gitlab",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/api",
        "//internal/tool/errz",
        "//internal/tool/httpz",
        "//internal/tool/tlstool",
        "@com_github_golang_jwt_jwt_v4//:jwt",
        "@com_github_hashicorp_go_retryablehttp//:go-retryablehttp",
        "@com_gitlab_gitlab_org_gitaly_v15//proto/go/gitalypb",
        "@io_opentelemetry_go_contrib_instrumentation_net_http_otelhttp//:otelhttp",
        "@io_opentelemetry_go_otel//:otel",
        "@io_opentelemetry_go_otel//propagation",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
    ],
)

go_custom_test(
    name = "gitlab_test",
    srcs = ["client_test.go"],
    deps = [
        ":gitlab",
        "//internal/tool/httpz",
        "//internal/tool/testing/mock_gitlab",
        "//internal/tool/testing/testhelpers",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
    ],
)
